
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Windows10
 */
public class TestCheckBox extends JFrame implements ActionListener {

    JLabel lbl1;
    JCheckBox chk1, chk2;
    JButton btn1;

    TestCheckBox() {
        lbl1 = new JLabel("Create shortcut desktop and shortcut in startup menu");
        lbl1.setSize(350, 20);
        lbl1.setLocation(100, 20);
        chk1 = new JCheckBox("Create shortcut in desktop");
        chk1.setSize(200, 20);
        chk1.setLocation(125, 50);
        chk2 = new JCheckBox("Create shortcut in startup menu");
        chk2.setSize(250, 20);
        chk2.setLocation(125, 80);
        btn1=new JButton("Confirm");
        btn1.setSize(100,20);
        btn1.setLocation(150,110);
        btn1.addActionListener(this);
        add(lbl1);
        add(chk1);
        add(chk2);
        add(btn1);
        setSize(500, 500);
        setLayout(null);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void actionPerformed(ActionEvent e) {
        String mes = "";
        if (chk1.isSelected()) {
            mes = ("Create shotcut in desktop: Complete\n");
        }
        if (chk2.isSelected()) {
            mes += ("Create shortcut in startup menu: Complete\n");
        }
        JOptionPane.showMessageDialog(this, mes);
    }

    public static void main(String[] args) {
        new TestCheckBox();
    }

}
