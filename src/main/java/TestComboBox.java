
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class TestComboBox extends JFrame implements ActionListener{
    JLabel lbl1;
    JComboBox cb1;
    JButton btn1;
    
    TestComboBox(){
        lbl1=new JLabel("Which team do you like?");
        lbl1.setSize(300, 20);
        lbl1.setLocation(125,20);
        String team[]={"OG","EG","Secret","Fnatic","Alliance","LGD","Nigma"};
        cb1=new JComboBox(team);
        cb1.setSize(200,20);
        cb1.setLocation(125, 50);
        btn1=new JButton("Confirm");
        btn1.setSize(100, 20);
        btn1.setLocation(125,80);
        btn1.addActionListener(this);
        add(lbl1);add(cb1);add(btn1);
        setSize(500,500);
        setLayout(null);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    public void actionPerformed(ActionEvent e){
        JOptionPane.showMessageDialog(this, cb1.getItemAt(cb1.getSelectedIndex())+" is Selected");
    }
    public static void main(String[] args) {
        new TestComboBox();
    }
}
