
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class TestList extends JFrame implements ActionListener{
    JLabel lbl1,lbl2;
    JButton btn1;
    DefaultListModel<String> l1;
    JList<String> list1; 
    TestList(){
        lbl1=new JLabel("Choose one that you like.");
        lbl1.setSize(200,20);
        lbl1.setLocation(125,20);
        lbl2=new JLabel();
        lbl2.setSize(200,20);
        lbl2.setLocation(125,200);
        l1=new DefaultListModel<>();
        l1.addElement("Spring");
        l1.addElement("Summer");
        l1.addElement("Autumn");
        l1.addElement("Winter");
        list1=new JList<>(l1);
        list1.setSize(100,80);
        list1.setLocation(125,50);
        btn1= new JButton("OK");
        btn1.setSize(100,20);
        btn1.setLocation(300,50);
        btn1.addActionListener(this);
        add(lbl1);add(lbl2);add(list1);add(btn1);
        setSize(500,500);
        setLayout(null);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    public void actionPerformed(ActionEvent e){
        String data="";
        if(list1.getSelectedIndex() != -1){
            data="You choose: "+list1.getSelectedValue();
            lbl2.setText(data);
        }
        lbl2.setText(data);
    }
    public static void main(String[] args) {
        new TestList();
    }
}
