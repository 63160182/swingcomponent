
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class TestMenuBar extends JFrame{
    JMenu menu, submenu;  
          JMenuItem i1, i2, i3, i4, i5;  
    TestMenuBar(){ 
          JFrame f= new JFrame("Menu and MenuItem Example");  
          JMenuBar mb=new JMenuBar();  
          menu=new JMenu("System");  
          submenu=new JMenu("Boost ISO(Full)");  
          i1=new JMenuItem("Boost ISO(Fast)");  
          i2=new JMenuItem("Run ELF");  
          i3=new JMenuItem("Enable cheat");  
          i4=new JMenuItem("Load state");  
          i5=new JMenuItem("Save state");  
          menu.add(i1); menu.add(i2); menu.add(i3);  
          submenu.add(i4); submenu.add(i5);  
          menu.add(submenu);  
          mb.add(menu);  
          f.setJMenuBar(mb);  
          f.setSize(400,400);  
          f.setLayout(null);  
          f.setVisible(true);  
    }
    public static void main(String[] args) {
        new TestMenuBar();
    }
}
