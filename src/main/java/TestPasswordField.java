
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class TestPasswordField implements ActionListener{
    public static void main(String[] args) {
        JFrame frame=new JFrame("Password");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500,500);
        frame.setLayout(null);
        
        JLabel lbl3=new JLabel();
        lbl3.setSize(300, 20);
        lbl3.setLocation(150,100);
        frame.add(lbl3);
        
        JLabel lbl4=new JLabel();
        lbl4.setSize(300, 20);
        lbl4.setLocation(150,120);
        frame.add(lbl4);
        
        JLabel lbl1=new JLabel("Username");
        lbl1.setSize(80,20);
        lbl1.setLocation(100,200);
        frame.add(lbl1);
        
        JTextField txt1=new JTextField();
        txt1.setSize(120,20);
        txt1.setLocation(180,200);
        frame.add(txt1);
        
        JLabel lbl2=new JLabel("Password");
        lbl2.setSize(80,20);
        lbl2.setLocation(100,250);
        frame.add(lbl2);
        
        JPasswordField pass1=new JPasswordField();
        pass1.setSize(120,20);
        pass1.setLocation(180, 250);
        frame.add(pass1);
        
        JButton btn1=new JButton("Login");
        btn1.setSize(80,20);
        btn1.setLocation(200,300 );
        frame.add(btn1);
        
        btn1.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                lbl3.setText("Username: "+txt1.getText());
                lbl4.setText("Password: "+new String(pass1.getPassword()));
            }
        });
        
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        
    }
}
