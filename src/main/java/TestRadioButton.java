
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class TestRadioButton extends JFrame implements ActionListener {
    JLabel lbl1;
    JRadioButton rad1,rad2;
    JButton btn1;
    TestRadioButton(){
        lbl1=new JLabel("Women");
        lbl1.setSize(100,20);
        lbl1.setLocation(125,20 );
        rad1=new JRadioButton("Yes");
        rad1.setSize(100,20);
        rad1.setLocation(125, 50);
        rad2=new JRadioButton("No");
        rad2.setSize(100,20);
        rad2.setLocation(125,80);
        btn1=new JButton("Confirm");
        ButtonGroup bg=new ButtonGroup();
        bg.add(rad1);bg.add(rad2);
        btn1.setSize(100,20);
        btn1.setLocation(150,110);
        btn1.addActionListener(this);
        add(lbl1);add(rad1);add(rad2);add(btn1);
        setSize(500,500);
        setLayout(null);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    public void actionPerformed(ActionEvent e){
        if(rad1.isSelected()){
            JOptionPane.showMessageDialog(this, "Go to kitchen");
        }
        if(rad2.isSelected()){
            JOptionPane.showMessageDialog(this, "Good");
        }
    }
    public static void main(String[] args) {
        new TestRadioButton();
    }
}
