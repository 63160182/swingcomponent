
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollBar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class TestScrollBar extends JFrame implements AdjustmentListener{
    JLabel lbl1;
    JScrollBar s1;
    TestScrollBar(){
        lbl1=new JLabel("Funny");
        lbl1.setSize(200,20);
        lbl1.setLocation(125,20);
        s1=new JScrollBar();
        s1.setSize(80,100);
        s1.setLocation(150,50);
        
        add(lbl1);add(s1);
        setSize(500,500);
        setLayout(null);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        s1.addAdjustmentListener(this);
    }
    
    public void adjustmentValueChanged(AdjustmentEvent e){
        lbl1.setText("Depress Level: "+s1.getValue());
    }
    public static void main(String[] args) {
        new TestScrollBar();
    }
}
