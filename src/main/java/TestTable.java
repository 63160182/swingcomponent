
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class TestTable extends JFrame{
    JTable jt1;
    JScrollPane sp1;
    TestTable(){
        String data[][]={{"001","Joe","15000"},
            {"002","Deez","20000"},
            {"003","Anucha","5000"}};
        String column[]={"ID","NAME","SALARY"};
        jt1=new JTable(data,column);
        jt1.setSize(300,300);
        jt1.setLocation(20, 20);
        sp1=new JScrollPane(jt1);
        add(sp1);
        setSize(500, 500);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        }
    public static void main(String[] args) {
        new TestTable();
    }
}
