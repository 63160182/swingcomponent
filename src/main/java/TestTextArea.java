
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Windows10
 */
public class TestTextArea implements ActionListener {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);
        frame.setLayout(null);

        JLabel lbl1 = new JLabel("Word:");
        lbl1.setSize(80, 20);
        lbl1.setLocation(50, 50);
        frame.add(lbl1);

        JLabel lbl2 = new JLabel("Characters:");
        lbl2.setSize(140, 20);
        lbl2.setLocation(250, 50);
        frame.add(lbl2);

        JButton btn1 = new JButton("Count Words");
        btn1.setSize(150, 20);
        btn1.setLocation(150, 300);
        frame.add(btn1);

        JTextArea ara = new JTextArea();
        ara.setSize(350, 150);
        ara.setLocation(50, 100);
        frame.add(ara);

        btn1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                String text = ara.getText();
                String words[] = text.split("\\s");
                lbl1.setText("Word: " + words.length);
                lbl2.setText("Characters: " + text.length());

            }
        });

        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
